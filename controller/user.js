'use strict';
const connection = require('../configs/database');
const ResponseUtil = require('../configs/responseUtils');

module.exports = {
    signUp: (event, context, callback) => {
        context.callbackWaitsForEmptyEventLoop = false;
        const userId = event.id;
        const profilePic = event.input.profilePic;
        const name = event.input.name;
        const provider = event.input.provider;

        getUser(userId)
            .then(userInfo => createUser(userInfo, userId, name, profilePic, provider))
            .then(result => {
                callback(null, ResponseUtil.getResultResponse(result));
            }).catch(error => {
                callback(null,ResponseUtil.getErrorResponse(error));
            });
    },

    getUserInfo: (event, context, callback) => {
        context.callbackWaitsForEmptyEventLoop = false;
        getUser(event.id).then(userInfo => {
            callback(null, ResponseUtil.getResultResponse(userInfo));
        }).catch(error => {
            callback(null, ResponseUtil.getErrorResponse(error));
        });

    }
}


function getUser(userId) {
    return new Promise((resolve, reject) => {
        const sql = 'SELECT * FROM users WHERE _id = ? LIMIT 1';
        connection.query(sql, [userId], function (err, result) {
            if (err) {
                reject(err);
            } else {
                resolve(result[0]);
            }

        });
    });
}

function createUser(userInfo, id, name, profilePic, provider) {
    return new Promise((resolve, reject) => {
        if (!id || !name || !provider) {
            return reject(ResponseUtil.getPramsInvalidErr());
        }

        if (userInfo) {
            return resolve(userInfo);
        }
        const createdAt = new Date().getTime();
        var value = [id, name, profilePic, 1, provider, createdAt]
        const sql = "INSERT INTO users (_id,name,profile_picture,redmine_notification,provider,created_at) VALUES (?,?,?,?,?,?)";
        connection.query(sql, value, (err, res) => {
            if (err) {
                reject(err);
            } else {
                resolve({
                    _id: id,
                    name: name,
                    profile_picture: profilePic,
                    redmine_notification: 1,
                    created_at: createdAt,
                    provider: provider
                });
            }

        });

    });
}

