'use strict';
const connection = require('../configs/database');
const ResponseUtils = require('../configs/responseUtils');
module.exports = {
  create: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const folderName = event.input.folderName;
    const description = event.input.description;
    createFolder(userId, folderName, description)
      .then(insertId => createUserFolder(userId, insertId))
      .then(folderId => getFolderDetail(folderId))
      .then(folderDetail => {
        callback(null, ResponseUtils.getResultResponse(folderDetail));
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });
  },

  list: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const limit = event.limit;
    const offset = event.offset;
    const userId = (event.targetId) ? event.targetId : event.id;
    list(userId, limit, offset)
      .then(results => {
        callback(null, ResponseUtils.getResultResponse(results));
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });
  },

  bookMarkFolder: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const folderId = event.input.folderId;
    getFolderDetail(folderId)
      .then(folderDetail => {
        createUserFolder(userId, folderId)
          .then(fodlerId => {
            callback(null, ResponseUtils.getResultResponse(folderDetail));
          }).catch(error => {
            callback(null, ResponseUtils.getErrorResponse(error));
          });

      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });
  },

  deleteFolder: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const folderId = event.input.folderId;
    deleteFolderUser(userId, folderId)
      .then(affectedRows => deleteFolder(userId, folderId))
      .then(affectedRows => deleteTestsFolder(userId, folderId))
      .then(affectedRows => deleteTestsUser(userId, folderId))
      .then(affectedRows => {
        callback(null, ResponseUtils.getResultResponse("folder deleted"));
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });

  },

  rename: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const folderId = event.input.folderId;
    const folderName = event.input.folderName;
    let data = [folderName, folderId, userId];
    let sql = "UPDATE folders SET name = ? WHERE _id= ? AND author_id = ?";
    connection.query(sql, data, (error, results, fields) => {
      if (error) {
        callback(null, ResponseUtils.getErrorResponse(error));
        return;
      }
      if (results.affectedRows == 0) {
        callback(null, ResponseUtils.getDeniedError());
        return;
      }
      callback(null, "Successful");
    })
  }

}

function deleteFolderUser(userId, folderId) {
  return new Promise((resolve, reject) => {
    var sql = "DELETE FROM folder_user WHERE folder_id = ? AND created_id = ?";
    let value = [folderId, userId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      resolve(results.affectedRows);
    });
  })
}


function deleteFolder(userId, folderId) {
  return new Promise((resolve, reject) => {
    var sql = "DELETE FROM folders WHERE _id = ? AND author_id = ?";
    let value = [folderId, userId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      resolve(results.affectedRows);
    });
  })
}



function deleteTestsFolder(userId, folderId) {
  return new Promise((resolve, reject) => {
    let sql = "DELETE FROM tests WHERE author_id = ? AND folder_id = ?";
    let value = [userId, folderId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      resolve(results.affectedRows);
    });
  });
}

function deleteTestsUser(userId, folderId) {
  return new Promise((resolve, reject) => {
    let sql = "DELETE FROM test_user WHERE created_id = ? AND folder_id = ?";
    let value = [userId, folderId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      resolve(results.affectedRows);
    });
  });
}


function getFolderDetail(folderId) {
  return new Promise((resolve, reject) => {
    if (!folderId) {
      return reject(ResponseUtils.getPramsInvalidErr());
    }
    var sql = "SELECT folders._id, folders.name as folder_name,folders.test_count, folders.author_id,folders.created_at,users.name as author_name,users.profile_picture FROM folders,users WHERE folders._id = ? AND folders.author_id = users._id LIMIT 1";
    let value = [folderId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        const element = results[0];
        if (!element) {
          return reject(ResponseUtils.getNotFoundErr());
        }
        let author = {
          id: element.author_id,
          name: element.author_name,
          profile_picture: element.profile_picture
        }
        delete element.author_id;
        delete element.profile_picture;
        delete element.author_name;
        element.author = author;
        resolve(element);
      }
    });
  });
}

function list(userId, limit, offset) {
  return new Promise((resolve, reject) => {
    if (!userId || !limit || !offset) {
      return reject(ResponseUtils.getPramsInvalidErr());
    }
    var sql = "SELECT folder_user.folder_id, folders.name as folder_name,folders.test_count, folders.author_id,folders.created_at,users.name as author_name,users.profile_picture FROM folder_user,folders,users WHERE folder_user.created_id =? AND folder_user.folder_id = folders._id AND folders.author_id = users._id ORDER BY folder_user.created_at ASC LIMIT ? OFFSET ?";
    let value = [userId, parseInt(limit, 10), parseInt(offset, 10)];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        results.forEach(element => {
          let author = {
            id: element.author_id,
            name: element.author_name,
            profile_picture: element.profile_picture
          }
          delete element.author_id;
          delete element.profile_picture;
          delete element.author_name;
          element.author = author;
        });
        resolve(results);
      }
    });
  });
}

function createFolder(authorId, folderName, description) {
  return new Promise((resolve, reject) => {
    if (!authorId || !folderName) {
      return reject(ResponseUtils.getPramsInvalidErr());
    }
    let stmt = "INSERT INTO folders (name, description,author_id, created_at) VALUES(?,?,?,?)";
    let value = [folderName, description, authorId, new Date().getTime()];
    connection.query(stmt, value, (err, results, fields) => {
      if (err) {
        return reject(err);
      }

      resolve(results.insertId);
    });
  });
}

function createUserFolder(createdId, folderId) {
  return new Promise((resolve, reject) => {
    if (!createdId || !folderId) {
      return reject(ResponseUtils.getPramsInvalidErr());
    }
    let stmt = "INSERT INTO folder_user (folder_id, created_id,created_at, last_used) VALUES(?,?,?,?)";
    let now = new Date().getTime();
    let value = [folderId, createdId, now, now];
    connection.query(stmt, value, (err, results, fields) => {
      if (err) {
        if (err.code == "ER_DUP_ENTRY") {
          return reject(ResponseUtils.getDupNameErr());
        }
        return reject(err);
      }
      resolve(folderId);
    });
  });
}

