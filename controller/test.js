'use strict';
const connection = require('../configs/database');
const ResponseUtils = require('../configs/responseUtils');

module.exports = {
  create: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const testTitle = event.input.title;
    const description = event.input.description;
    const folderId = event.input.folderId;
    const tasks = event.input.tasks;

    createTest(userId, folderId, testTitle, description, tasks)
      .then(insertId => createTestUser(userId, insertId, folderId))
      .then(testId => getTestItem(testId))
      .then(testItem => {
        callback(null, ResponseUtils.getResultResponse(testItem));
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });
  },

  list: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const limit = event.limit;
    const offset = event.offset;
    const userId = (event.targetId) ? event.targetId : event.id;
    const folderId = (event.folderId) ? event.folderId : 0;
    list(userId, folderId, limit, offset)
      .then(results => {
        callback(null, ResponseUtils.getResultResponse(results));
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });
  },

  upadteTestResult: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const testId = event.input.testId;
    const result = JSON.stringify(event.input.result);
    let data = [result, testId, userId];
    let sql = "UPDATE test_user SET test_result = ? WHERE test_id= ? AND created_id = ?";
    connection.query(sql, data, (error, results, fields) => {
      if (error) {
        callback(null, ResponseUtils.getErrorResponse(error));
        return;
      }
      callback(null, callback(null, ResponseUtils.getResultResponse("successful")));
    });
  },

  updateStudyResult: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const testId = event.input.testId;
    const result = JSON.stringify(event.input.result);
    let data = [result, testId, userId];
    let sql = "UPDATE test_user SET study_result = ? WHERE test_id= ? AND created_id = ?";
    connection.query(sql, data, (error, results, fields) => {
      if (error) {
        callback(null, ResponseUtils.getErrorResponse(error));
        return;
      }
     callback(null, callback(null, ResponseUtils.getResultResponse("successful")));
    });
  },


  bookmarkTest: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const testId = event.input.testId;
    const folderId = event.input.folderId;
    if (folderId) {
      isFolderAuthor(folderId, userId).then(result => {
        if (result) {//is author
          createTestUser(userId, testId, folderId).then(_testId => {
            callback(null, ResponseUtils.getResultResponse(_testId));
          }).catch(error => {
            callback(null, ResponseUtils.getErrorResponse(error));
          });
        } else {
          callback(null, ResponseUtils.getDeniedError());
        }
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      });
    } else {
      createTestUser(userId, testId, 0).then(_testId => {
        callback(null, callback(null, ResponseUtils.getResultResponse(_testId)));
      }).catch(error => {
        callback(null, callback(null, ResponseUtils.getErrorResponse(error)));
      });
    }
  },

  delete: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const userId = event.id;
    const testId = event.input.testId;
    const folderId = (event.input.folderId) ? event.input.folderId : 0;
    deleteTestUser(userId, testId, folderId)
      .then(affectedRows => deleteTest(userId, testId, folderId))
      .then(affectedRows => {
        deleteTestInFolder(testId);
        callback(null, ResponseUtils.getResultResponse("Test deleted"));
      }).catch(error => {
        callback(null, ResponseUtils.getErrorResponse(error));
      })
  },

  detail: (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const testId = event.testId;
    let sql = "SELECT tests.description, tests.created_at, tests.tasks, test_user.test_result, test_user.study_result FROM tests, test_user WHERE tests._id = ? AND tests._id = test_user.test_id LIMIT 1";
    let value = [testId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        callback(null, ResponseUtils.getErrorResponse(error));
        return;
      }
      if (results.length ==0) {
         callback(null, ResponseUtils.getNotFoundErr());
         return;
      }
      var element = results[0];
      callback(null, ResponseUtils.getResultResponse(element));
    });
  }

}


function deleteTestUser(userId, testId, folderId) {
  return new Promise((resolve, reject) => {
    var sql = "DELETE FROM test_user WHERE test_id = ? AND folder_id = ? AND created_id = ?";
    let value = [testId, folderId, userId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      resolve(results.affectedRows);
    });
  })
}

function deleteTest(userId, testId, folderId) {
  return new Promise((resolve, reject) => {
    var sql = "DELETE FROM tests WHERE _id = ? AND author_id = ?";
    let value = [testId, userId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      if (folderId != 0 && results.affectedRows && results.affectedRows > 0) {
        decreaseFolderTestCount(folderId);
      }
      resolve(results.affectedRows);
    });
  })
}


function isFolderAuthor(folderId, userId) {
  return new Promise((resolve, reject) => {
    var sql = "SELECT COUNT(*) AS count FROM folders WHERE _id = ? AND author_id = ?";
    let value = [folderId, userId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        if (results.length == 0) {
          resolve(false);
        } else {
          resolve(true);
        }
      }

    });
  });

}

function getTestItem(testId) {
  return new Promise((resolve, reject) => {
    if (!testId) {
      return reject(ResponseUtils.getPramsInvalidErr());
    }
    var sql = "SELECT tests._id, tests.title as test_title,tests.author_id,tests.description,tests.created_at,users.name as author_name,users.profile_picture FROM tests,users WHERE tests._id = ? AND tests.author_id = users._id LIMIT 1";
    let value = [testId];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }

      
      if (results.length ==0) {
        return reject(ResponseUtils.getNotFoundErr());
      }
      var element = results[0];
      let author = {
        id: element.author_id,
        name: element.author_name,
        profile_picture: element.profile_picture
      }
      delete element.author_id;
      delete element.profile_picture;
      delete element.author_name;
      element.author = author;
      resolve(element);
    });
  })
}

function list(userId, folderId, limit, offset) {
  return new Promise((resolve, reject) => {
    if (!userId || !limit || !offset) {
      return reject(ResponseUtils.getPramsInvalidErr());
    }
    var sql = "SELECT tests._id, tests.title as test_name,tests.author_id,tests.description,tests.created_at,users.name as author_name,users.profile_picture FROM test_user,tests,users WHERE test_user.folder_id = ? AND test_user.created_id =? AND test_user.test_id = tests._id AND tests.author_id = users._id ORDER BY test_user.created_at ASC LIMIT ? OFFSET ?";
    let value = [folderId, userId, parseInt(limit, 10), parseInt(offset, 10)];
    connection.query(sql, value, (error, results, fields) => {
      if (error) {
        return reject(error);
      }
      results.forEach(element => {
        let author = {
          id: element.author_id,
          name: element.author_name,
          profile_picture: element.profile_picture
        }
        delete element.author_id;
        delete element.profile_picture;
        delete element.authorName;
        element.author = author;
      });
      resolve(results);
    });
  })
}


function createTest(authorId, folderId, testName, des, tasks) {
  return new Promise((resolve, reject) => {
    let validateError = validateTestInput(authorId, testName, tasks);
    if (validateError) {
      return reject(validateError);
    }
    if (!folderId) {
      folderId = 0;
    }
    let stmt = "INSERT INTO tests (title,folder_id, author_id,description,tasks, created_at) VALUES(?,?,?,?,?,?)";
    let value = [testName, folderId, authorId, des, JSON.stringify(tasks), new Date().getTime()];
    connection.query(stmt, value, (err, results, fields) => {
      if (err) {
        reject(err);
      } else {
        if (folderId != 0) {
          increaseFolderTestCount(folderId);
        }
        resolve(results.insertId);
      }
    });
  });
}

function increaseFolderTestCount(folderId) {
  let data = [folderId];
  let sql = "UPDATE folders SET test_count = test_count + 1 WHERE _id= ?";
  connection.query(sql, data, (error, results, fields) => {
  })
}
function decreaseFolderTestCount(folderId) {
  let data = [folderId];
  let sql = "UPDATE folders SET test_count = test_count - 1 WHERE _id= ? AND test_count > 0";
  connection.query(sql, data, (error, results, fields) => {
  })
}

function validateTestInput(authorId, testName, tasks) {
  if (!authorId || !testName || !tasks || tasks.length == 0) {
    return {
      statusCode: 409,
      errMessage: "Invalid test title or tasks"
    }
  }
  for (var i = 0; i < tasks.length; i++) {
    var task = tasks[i];
    if (!task.taskId || !task.type || !Array.isArray(task.answers) || !task.answers || task.answers.length == 0) {
      return {
        statusCode: 409,
        errMessage: "Invalid task id or task type or task answers"
      }
    }
    var checkRightCount = 0;
    for (var j = 0; j < task.answers.length; j++) {
      var answer = task.answers[j];
      if (answer.rightAnswer) {
        checkRightCount++;
      }
      if (!answer.text) {
        return {
          statusCode: 409,
          errMessage: "Invalid task text"
        }
      }
    }
    if (checkRightCount == 0) {
      return {
        statusCode: 409,
        errMessage: "Select at least one correct answer"
      }
    }
  }
  return null;
}



function createTestUser(createdId, testId, folderId) {
  return new Promise((resolve, reject) => {
    folderId = (folderId) ? folderId : 0;
    let stmt = "INSERT INTO test_user (test_id, created_id, folder_id, created_at, last_used) VALUES(?,?,?,?,?)";
    let now = new Date().getTime();
    let value = [testId, createdId, folderId, now, now];
    connection.query(stmt, value, (err, results, fields) => {
      if (err) {
        reject(err);
      } else {
        resolve(testId);
      }
    });
  });
}

