const STATUS_CODE_OTHER_ERROR = 600;
const STATUS_CODE_INVALID_PARAMS = 601;
const STATUS_CODE_ITEM_NOT_FOUND = 602;
const STATUS_CODE_DUPLICATE_NAME = 603;
const STATUS_CODE_PERMISSION_DENIED = 604;

module.exports = {
    getPramsInvalidErr() {
        return {
            statusCode: STATUS_CODE_INVALID_PARAMS,
            errMessage: "Invalid or empty parameters"
        }
    },

    getNotFoundErr() {
        return {
            statusCode: STATUS_CODE_ITEM_NOT_FOUND,
            errMessage: "Item Not found"
        }
    },

    getDupNameErr() {
        return {
            statusCode: STATUS_CODE_DUPLICATE_NAME,
            errMessage: "Duplicate item"
        }
    },

    getDeniedError() {
        return {
            statusCode: STATUS_CODE_PERMISSION_DENIED,
            errMessage: "Permission denied"
        }
    },

    //error response format
    getErrorResponse(error) {
        if (error.statusCode) {
            return error;
        }
        return {
            statusCode: STATUS_CODE_OTHER_ERROR,
            error: error
        };
    },

    //success response format
    getResultResponse(result) {
        if (Array.isArray(result)) {
            return {
                statusCode: 200,
                results: result
            }
        } else {
            return {
                statusCode: 200,
                result: result
            }
        }
    }
}